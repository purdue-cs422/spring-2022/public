############################################################################
##
##     This file is part of Purdue CS 422.
##
##     Purdue CS 422 is free software: you can redistribute it and/or modify
##     it under the terms of the GNU General Public License as published by
##     the Free Software Foundation, either version 3 of the License, or
##     (at your option) any later version.
##
##     Purdue CS 422 is distributed in the hope that it will be useful,
##     but WITHOUT ANY WARRANTY; without even the implied warranty of
##     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##     GNU General Public License for more details.
##
##     You should have received a copy of the GNU General Public License
##     along with Purdue CS 422. If not, see <https://www.gnu.org/licenses/>.
##
#############################################################################

####################################################################
############### Set up Mininet and Controller ######################
####################################################################

SCRIPTS = ../../assignments/scripts
ASSIGNMENT0 = ../../assignments/assignment0

.PHONY: mininet controller cli netcfg host-h1 host-h2 host-h3 host-h4

mininet:
	$(SCRIPTS)/mn-stratum --topo single,4

controller:
	ONOS_APPS=gui,drivers.bmv2,lldpprovider,hostprovider,fwd \
	$(SCRIPTS)/onos

cli:
	$(SCRIPTS)/onos-cli

netcfg:
	ONOS_NETCFG_MOUNT_DIR=$(realpath $(ASSIGNMENT0)) \
	$(SCRIPTS)/onos-netcfg cfg/netcfg.json

host-h1:
	$(SCRIPTS)/utils/mn-stratum/exec h1

host-h2:
	$(SCRIPTS)/utils/mn-stratum/exec h2

host-h3:
	$(SCRIPTS)/utils/mn-stratum/exec h3

host-h4:
	$(SCRIPTS)/utils/mn-stratum/exec h4
